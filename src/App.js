import React, { Component } from 'react';
import { FormGroup, Label, Input, Modal, ModalHeader, ModalBody, ModalFooter, Table,  Button } from 'reactstrap';
import { CountryDropdown } from 'react-country-region-selector';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
 

class App extends Component {
 state = {
   students: [],
   newStudentData: {
     firstName: "",
     lastName: "",
     dateOfBirth: new Date(),
     nationality: {
      ID: "",
      Title: ""
    },
    country: ""
   },

   editStudentData: {
     ID: "",
     firstName: "",
     lastName: "",
     dateOfBirth: new Date(),
     nationality: {
      ID: "",
      Title: ""
    },
    country: ""
   },

   editStudentModal: false,
   newStudentModal: false
 }

 //Date Picker
 changeDate(date){
   this.setState({dateOfBirth: date})
 }

 //Nationality dropdown
 selectCountry(val){
   this.setState({country: val});
 }

// Fetching from API
 componentWillMount(){
   this.reloadModal();
 }

 // close student modal
  toggleNewStudentModal() {
   this.setState({
     newStudentModal : ! this.state.newStudentModal
   });
 }

 // add new student
 addNewStudent(){
   let {newStudentData} = this.state;
   fetch('http://localhost:8088/api/Students',{
     method: 'POST',
     headers: {'Content-Type':'application/json'},
     body: JSON.stringify({newStudentData}),
   }).then((response) => {

     let { students } = this.state;

     students.push(response.json());

     this.setState({ students,
       newStudentModal: false,
       newStudentData: {
         firstName: "",
         lastName: "",
         dateOfBirth: "",
         nationality: {
          ID: "",
          Title: ""
         }
        } 
      });
   });
 }


 // Close modal for Editing student details
 toggleEditStudentModal() {
   this.setState({
     editStudentModal : ! this.state.editStudentModal
   });
 }


 //Edit student details
 editStudentDetails(ID, firstName, lastName, dateOfBirth, nationality){
  this.setState({
    editStudentData: {ID, firstName, lastName, dateOfBirth, nationality},
    editStudentModal :  this.state.editStudentModal
  });
}

 //Update student details
 updateStudentDetails(){
   let {firstName, lastName, dateOfBirth, nationality} = this.state.editStudentData;
   fetch('http://localhost:8088/api/Students' + this.state.editStudentData.ID,
   {firstName, lastName, dateOfBirth, nationality}, {
     method: 'PUT',
     headers: {'Content-Type':'application/json'},
     body: JSON.stringify({firstName, lastName, dateOfBirth, nationality}),
   })
   .then((response) => {
    this.setState({ students : response.json(),
     editStudentModal: false,
     editStudentData: {
       ID:"",
       firstName: "",
       lastName: "",
       dateOfBirth: "",
       nationality: {
         ID: "",
         Title: ""
       }
     }
   });
   });
 }


//Deleting Student Details

 deleteStudent(ID){
   fetch('http://localhost:8088/api/Students' + ID,{
     method: 'DELETE',
     headers: {'Content-Type': 'application/json', },
     })
     .then((response) =>
       this.setState({ students: response })
    )};
  }


//Fetching from API
  reloadModal(){
   fetch('http://localhost:8088/api/Students')
   .then((Response)=>Response.json())
   .then((response) => {
     this.setState({ students: response })
   });
  }


 render() {

   let students = this.state.students.map((student) => {
     return (
       <tr key={student.ID}>
         <td>{student.ID}</td>
         <td>{student.firstName}</td>
         <td>{student.lastName}</td>
         <td>{student.dateOfBirth}</td>
         <td>
           <Button color="success" size="sm" className="mr-2" onClick={()=>this.toggleEditStudentModal(student.id, student.firstName, student.lastName, student.dateOfBirth, student.nationality)}>Edit</Button>
           <Button color="danger" size="sm" className="ml-2" onClick={()=>this.deleteStudent(student.ID)}>Delete</Button>
         </td>
       </tr>
     )
   });

   return (
     <div className="App container">


     {/* New Student Modal */}
     <Modal isOpen={this.state.newStudentModal} toggle={()=>this.toggleNewStudentModal()}>

       <ModalHeader toggle={()=>this.toggleNewStudentModal()}>New Student</ModalHeader>

       <ModalBody>
         <h6>Section 1: Basic Student Details</h6>

         <FormGroup>
           <Label for="firstName">First Name</Label>
           <Input id="firstName"
           value={this.state.newStudentData.firstName}
           onChange={(e) => {
             let { newStudentData } = this.state;
             newStudentData.firstName = e.target.value;
             this.setState({ newStudentData })
           }}/>
         </FormGroup>

         <FormGroup>
          <Label for="lastName">Last Name</Label>
           <Input id="lastName"
           value={this.state.newStudentData.lastName}
           onChange={(e) => {
             let { newStudentData } = this.state;
             newStudentData.lastName = e.target.value;
             this.setState({ newStudentData })
           }}/>
         </FormGroup>

         <Label for="Date Of Birth">Date Of Birth</Label>{' '}
         <DatePicker
            selected={this.state.dateOfBirth}
            onChange={(date)=>this.changeDate(date)} /><br/>

         <Label for="nationality">Nationality</Label>{' '}
         <CountryDropdown
            value={this.state.country}
            onChange={(val) => this.selectCountry(val)} />
       </ModalBody>

       <ModalFooter>
         <Button color="primary" onClick={()=>this.addNewStudent()}>Submit</Button>{' '}
         <Button color="secondary" onClick={() => this.toggleNewStudentModal()}>Cancel</Button>
       </ModalFooter>
     </Modal>
      {/* End of New Student Modal */}


     {/* Edit Student Details Modal */}
     <Modal isOpen={this.state.editStudentModal} toggle={()=>this.toggleEditStudentModal()}>
       
       <ModalHeader toggle={()=>this.toggleEditStudentModal()}>Student Details</ModalHeader>
       
       <ModalBody>
         <h6>Section 1: Basic Student Details</h6>

         <FormGroup>
           <Label for="firstName">First Name</Label>
           <Input id="firstName"
           value={this.state.editStudentData.firstName}
           onChange={(e) => {
             let { editStudentData } = this.state;
             editStudentData.firstName = e.target.value;
             this.setState({ editStudentData })
           }}/>
         </FormGroup>

         <FormGroup>
          <Label for="lastName">Last Name</Label>
           <Input id="lastName"
           value={this.state.editStudentData.lastName}
           onChange={(e) => {
             let { editStudentData } = this.state;
             editStudentData.lastName = e.target.value;
             this.setState({ editStudentData })
           }}/>
         </FormGroup>


         <Label for="Date Of Birth">Date Of Birth</Label>{' '}
         <DatePicker
         selected={this.state.dateOfBirth}
         onChange={(date)=>this.changeDate(date)} /> <br/>


         <Label for="nationality">Nationality</Label>{' '}
         <CountryDropdown
            value={this.state.country}
            onChange={(val) => this.selectCountry(val)} />   

            <h6>Section 2: Family Details </h6>
          <br/>

          <FormGroup>
            <Label for="fullName">Full Name</Label> 
            <Input type="text" required="true" placeholder="Full Name" />
          </FormGroup>

          <FormGroup>
            <Label for="relation">Relation</Label>{' '}
            <Input type="select" name="backdrop" id="backdrop" onChange={this.changeBackdrop}>
              <option value="parent">Parent</option>
              <option value="sibling">Sibling</option>
              <option value="spouse">Spouse</option>
            </Input>
          </FormGroup>  

       </ModalBody>

       <ModalFooter>
         <Button color="primary" onClick={()=>this.editStudentDetails()}>Update</Button>{' '}
         <Button color="secondary" onClick={() => this.toggleEditStudentModal()}>Cancel</Button>
       </ModalFooter>
     </Modal>
    {/* End of Edit Student Details Modal */}


    {/* Roles Drop Down */}
     <header className="mb-2 mt-4">

      <FormGroup >
         <Label for="backdrop">Roles</Label>{' '}
         <Input type="select" name="backdrop" id="backdrop" onChange={this.changeBackdrop}>
           <option id="registrar">Registrar</option>
           <option id="admin">Admin</option>
         </Input>
       </FormGroup>
      {/* End of Roles Drop Down */}



       <h3>STUDENT REGISTRATION SYSTEM</h3><br/>
     </header>
       <Table>
         <thead>
           <tr>
             <th>ID</th>
             <th>First Name</th>
             <th>Last Name</th>
             <th>Date Of Birth</th>
           </tr>
         </thead>
         <tbody>
           {students}
         </tbody>
       </Table>
       <Button color="primary" size="sm" onClick={()=>this.toggleNewStudentModal()}> Add New Student </Button>
     </div>
    );
  }
}

export default App;


